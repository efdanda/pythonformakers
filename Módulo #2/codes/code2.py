#!/usr/bin/env python
# -*- coding: latin1 -*-

# Uma linha quebrada por contra-barra
A=7*3+\
5/2
# Uma lista (quebrada por vírgula)
b = ['a', 'b', 'c',
'd', 'e']

# Uma chamada de função (quebrada por vírgula)
c = range(1,
11)
