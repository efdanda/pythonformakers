#/usr/bin/python
#!-*- coding: latin1 -*-

# A função raw_input(“string”) é utilizada para atribuir
# valores a variáveis como gets() ou scanf() em C

idade = input("Digite sua idade\r\n")

if (idade>=18):
	print "Você pode assistir este filme"
elif(18>idade>=16):
	print "Peça Permissão a seus pais"
else:
	print "Você não pode assitir de jeito nenhum"
