#/usr/bin/python
#!-*- coding:utf8 -*

#Este código trata de um pequeno sistema de cadastramento e listagem de alunos em 
#uma sala da aula. Ele utiliza apenas dicionários.

#Autor: Everton Danda
#Ano: 2017


print "Alunos cadastro V0.0\n"

saida = False
sala = {}
id = 0
while (not saida):
	comando = raw_input("Por favor entre com o comando desejado: \n\n\n c - cadastro de aluno\n l - listagem dos alunos\n p - procura aluno pelo id\n d - deletar pelo id\n e - encerrar programa\n\n\n")

	if (comando == 'c'):
		nome = raw_input("Por favor insira o nome do aluno: ")
		idade = input("Profavor insira a idade do aluno: ")
		confirm = raw_input("Tem certeza que deseja incluir o aluno na sala? (y/n)\n\n-----Dados-----\nNome: "+nome+"\nIdade: "+str(idade)+"\n")
		if confirm.__contains__("y"):
			sala[id] = {'nome': nome, 'idade': idade}
			id += 1
			print "Aluno cadastrado!!!"

	elif (comando == 'l'):
		print "ID\n"
		for i,j in sala.items():
			print i, "=>", j
	elif (comando == 'p'):
		idp = intput("Entre com o Id do aluno: \n")
		print sala[idp]

	elif (comando =='d'):
		idp = input("Entre com o Id do aluno: \n")
		confirm = raw_input("Tem certeza que deseja deletar o aluno da lista?\n(y/n)\n")
		if confirm.__contains__("y") and sala.has_key(idp):
			del sala[idp]
		else:
			print "Usuário não encontrado"
	elif (comando =='e'):
		saida = True
		print "Obrigado por usar o Cadastro de Alunos\n"


