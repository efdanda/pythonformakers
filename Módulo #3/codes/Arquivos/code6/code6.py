#!/usr/bin/python
#-*- coding: utf8 -*-
import time
import datetime
import random
import os.path
import os

os.system("clear")

print """Este é um simples BOT de leitura e armazenamento de dados de sensores lidos"""

if (raw_input("Deseja Prosseguir? (Y/N)") == "n"):
	exit()

time.sleep(0.5)

print """Verificando se o banco de dados existe...\r\n \r\n"""

time.sleep(0.5)

if not os.path.isfile("arquivo.db"):
	print """arquivo.db inexistente... a criar novo arquivo...\r\n\r\n"""
	open("arquivo.db", "w").close()
	time.sleep(1.0)
	print """arquivo.db criado com sucesso!\r\n\r\n"""
	time.sleep(1.0)

print """S - para começar a leitura e armazenamento; \r\n
P - Pesquisar medidas;\r\n
L - Limpar banco de dados\r\n"""

resp = raw_input("")

if resp == "P":
	print "Pesquisando Banco...\r\n"
	time.sleep(0.5)
	arquivo = open("arquivo.db", "r")
	dados = eval(arquivo.read())
	print "Exibindo Dados: \r\n"
	time.sleep(0.5)
	for i in dados:
		print i
		time.sleep(0.1)
	arquivo.close()

elif resp == "L":
	print "Apagando dados... \r\n"
	time.sleep(0.5)
	open("arquivo.db","w").close()
	print "arquivo.db limpo...\r\n"
	time.sleep(0.5)

elif resp == "S":
	arquivo = open("arquivo.db","r")
	if os.stat("arquivo.db").st_size == 0:
		dados = []
	else:
		dados = eval(arquivo.read())
	arquivo.close()
#	arquivo = open("arquivo.db","r")
#	dados = eval(arquivo.read())
#	aqruivo.close()
	numero_de_leituras = input("Informe o número de leituras desejadas...")
	contador = numero_de_leituras
	print "Iniciando leitura!\r\n"
	time.sleep(0.5)
	while contador:
		dicionario = {"data":(datetime.datetime.now()-datetime.datetime(1970,1,1)).total_seconds(), "temperatura":random.randrange(25,30), "deslocamento":random.randrange(0,30), "nivel": random.randrange(5,6)}
		print dicionario
		dados.append(dicionario)
		time.sleep(1)
		contador -= 1
	print "Leituras encerradas...\r\n"
	time.sleep(0.5)
	print "Armazenando dados...\r\n"
	time.sleep(0.5)
	arquivo = open("arquivo.db", "w")
	arquivo.write(str(dados))
	arquivo.close()
	print "Dados salvos com suscesso!\r\nTotal de Amostras: "+ str(numero_de_leituras)
else:
	exit()
