#!/usr/bin/python
#-*- coding: utf8 -*-

import traceback

try: import sqlalchemy
except ImportError:
	trace = traceback.format_exc()

	print "Erro de importação: %s, favor checar o o arquivo trace.log", trace

	open('trace.log', 'a').write(trace)
