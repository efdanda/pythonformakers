#!/usr/bin/python
#-*- coding: utf8 -*-

import traceback
import time

try: import sqlalchemy
except ImportError:
	trace = traceback.format_exc()

	print "Erro de importação: %s, favor checar o o arquivo trace.log", trace

	ermsg = "\r\n"+time.asctime()+" - Erro: \r\n"+trace

	open('trace.log', 'a').write(ermsg)
